//alert("B249!");

//In JS, classes can be created using the "class" keyword and {}.
//Naming convention for classes: Begin with Uppercase characters

/*
	Syntax:

		class <Name> {
		
		}

*/

//here we have an empty student class
class Student {
	constructor(name, email) {
		//propertyName = value
		this.name = name;
		this.email = email;
	}
}

//Instantiation - process of creating objects from class
//To create an object from a class, use the "new" keyword, when a class has a constructor, we need to supply ALL the values needed by the constructor
let studentOne = new Student('john', 'john@mail.com');
console.log(studentOne);


/*
	Mini-Exercise: 
 
		Create a new class called Person.

		This Person class should be able to instantiate a new object with the ff fields:

		name,
		age, (should be a number and must be a greater than or equal to 18, otherwise, set the property to undefined)
		nationality,
		address
	
		Instantiate 2 new objects from the Person class as person1 and person2

		Log both objects in the console. Take a screenshot of your console and send it to our groupchat.

*/

class Person {
	constructor(name, age, nationality, address) {
		this.name = name;
		this.nationality = nationality;
		this.address = address;
		if (typeof age === "number" && age >=18) {
			this.age = age;
		} else {
			this.age = undefined;
		}
	}

}

let person1 = new Person("Wednesday Addams", 16, "American", "LA");
let person2 = new Person("Enid Sinclair", 18, "American", "San Francisco");

// console.log(person2);
// console.log(person1);

// class Dog {

// }

// let dog1 = new Dog();
// console.log(dog1);

/*
	1. Classes or constructor/prototype
	2. Pascal
	3. New
	4. Instantiation
  5. Constructor() method

*/